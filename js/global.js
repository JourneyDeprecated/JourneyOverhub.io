$(function() {
 var headerHeight = $('.nav-container').outerHeight();

 $('.slide-nav').click(function(e) {

	  var linkHref = $(this).attr('href');

	  $('html, body').animate({
		  scrollTop: $(linkHref).offset().top - headerHeight
	  }, 1000);

	  e.preventDefault();
 })
});

$(function(){
	$(window).scroll(function() {
		 if ($(this).scrollTop() >= 500) {
			  $('.main-nav').addClass('stickytop');
		 }
		 else {
			  $('.main-nav').removeClass('stickytop');
		 }
		 if ($(this).scrollTop() >= 500) {
			  $('.nav').addClass('removeBorder');
		 }
		 else {
			  $('.nav').removeClass('removeBorder');
		 }
	});
});

/* $(function() {
	  var slides = document.querySelectorAll('#slides .slide');
	  var currentSlide = 0;
	  var slideInterval = setInterval(nextSlide,10000);

  function nextSlide() {
		   slides[currentSlide].className = 'slide';
		   currentSlide = (currentSlide+1)%slides.length;
		   slides[currentSlide].className = 'slide showing';
	  }
  }); */

$(function(){
	function _(id){ return document.getElementById(id); }
	function submitForm(){
	_("mybtn").disabled = true;
	_("status").innerHTML = 'please wait ...';
	var formdata = new FormData();
	formdata.append( "n", _("n").value );
	formdata.append( "e", _("e").value );
	formdata.append( "m", _("m").value );
	var ajax = new XMLHttpRequest();
	ajax.open( "POST", "example_parser.php" );
	ajax.onreadystatechange = function() {
		if(ajax.readyState == 4 && ajax.status == 200) {
			if(ajax.responseText == "success"){
				_("my_form").innerHTML = '<h2>Thanks '+_("n").value+', your message has been sent.</h2>';
			} else {
				_("status").innerHTML = ajax.responseText;
				_("mybtn").disabled = false;
			}
		}
	}
	ajax.send( formdata );
}
});

$(function() {
   $( "#phone-btn" ).click(function() {
      $( ".phone-nav" ).slideToggle();
 });
});

$(window).load(function() {
	$(".se-pre-con").fadeOut("slow");;
});